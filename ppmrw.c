#include <stdlib.h>
#include <stdio.h>
#include <string.h>

//Thomas Back Ppm converter

typedef struct pixel {
  unsigned char r, g, b;
} pixel;



int width, height, first_arg, maxvalue;
int buff_size[65];

int i , j;
int c;






//convert to p3 function TO DO
void p3_convert(pixel to_write[], char* file2write, int w, int h, int mv){
	
	//open file for writing
	FILE* fw = fopen(file2write, "wb");
	
	fprintf(fw, "P3 %d %d %d \n", h, w, mv  ); //ADD file to header
	for(i = 0; i < w * h; i += 1){
		fprintf(fw, "%d %d %d \n", to_write[i].r, to_write[i].g, to_write[i].b);
	}
	
	fclose(fw);
	
	
	
}

//convert to p6 function TO DO
void p6_convert(pixel to_write[],  char* file2write, int w, int h, int mv){
	
	//open file for writing
	FILE* fw = fopen(file2write, "wb");
	
	fprintf(fw, "P6 %d %d %d \n", h, w, mv ); //Add header information to file
	fwrite(to_write, sizeof(pixel), h * w, fw);
	
	fclose(fw);
	
}


int main(int argc, char *argv[]) {
	
	
	
	//check to see there are 4 arguments submitted
	if (argc != 4){
		fprintf( stderr, "Error: Please include 3 arguments, P version, input file, and output file name");
		exit(0);
	}
	
	
	//ignore argv[0]
	
	//read file into fh
	FILE* fh = fopen(argv[2], "r");
	
	//error message if file doesnt exist
	if (!fh){
		fprintf(stderr, "Error: Could not find file to be opened");
		exit(0);
	}
	
	printf("got here");
	//first read header file and determine if it is p6 or p3, reads first character, if it is  P, we advance to see if it is 3 or 6 following.
	int mn = fgetc(fh);
	
	if (mn != 'P'){
		fprintf(stderr, "Error: Magic number is not correct format, must begin with P");
		exit(0);
	}
	printf("here");	
	
	
	//repeat fgetc so it indexes to next position and grabs magic number
	mn = fgetc(fh);
	
	
	//skip past comments to find width and height 
	c = fgetc(fh);
	while(c == '\n' | c == ' '){
		printf("got here");
		//loop out of newline to get to comments
		c = fgetc(fh);
	}
	printf("got here1");
	if(c =='#'){
		printf("is it the comment while loop?");
		while(c != '\n'){
			if(c == '\n'){
				break;
			}
			c = fgetc(fh);
		}
	}
	
	// read in height and width and use it to allocate space for image
	printf("After");
	//first read in height
	for(i=0; i < 64; i += 1){
		buff_size[i] = fgetc(fh);
		if(buff_size[i] == ' '){
			buff_size[i] = 0;
			break;
		}
	}
	printf("After");
	//store height
	height = atoi(buff_size);
	
	buff_size[64];
	
	//now read and store width,
	for(i=0; i < 64; i += 1){
		buff_size[i] = fgetc(fh);
		if(strcmp(buff_size[i], ' ')){
			buff_size[i] = 0;
			break;
		}
	}
	
	//store width
	width = atoi(buff_size);
	
	buff_size[64];
	//read and store max value
	for(i=0; i < 64; i += 1){
		buff_size[i] = fgetc(fh);
		if(strcmp(buff_size[i], ' ')){
			buff_size[i] = 0;
			break;
		}
		//if we leave the bounds of the for loop
		if(i == 64){
			fprintf(stderr, "Error: Header does not contain valid information.");
			exit(0);
		}
	}
	
	//store maxvalue and check that is within the bounds of 0-255 for my program
	maxvalue = atoi(buff_size);
	if(maxvalue > 255 | maxvalue < 0){
		fprintf(stderr, "Error: maxvalue for this program must be between 0 and 225, please enter a valid ppm file.");
		exit(0);
	}
	
	//allocate memory to work with in pixmap
	pixel pixmap[width * height];
	
	char buff[64];	
	
	
	//if file format is P3
	if (mn == "3"){
		
		//outer for loop to loop through size of pixmap array
		for(i = 0; i < width * height; i += 1){
			
				//loop to put into red value
				for(j=0; j < 65; j += 1){
					
					//obtain first red value
					buff[j] = fgetc(fh);
					
					//break when we hit a space
					if(strcmp(buff[j], " ")){
						buff[j] = 0;
						break;
					}
				}
				pixmap[i].r = atoi(buff);
				
				//loop to put into blue value
				for(j=0; j < 65; j += 1){
					
					//obtain first blue value
					buff[j] = fgetc(fh);
					
					//break when we hit a space
					if(strcmp(buff[j], " ")){
						buff[j] = 0;
						break;
					}
				}
				
				pixmap[i].b = atoi(buff);
				
				//loop to put into green value
				for(j=0; j < 65; j += 1){
					
					//obtain first green value
					buff[j] = fgetc(fh);
					
					//break when we hit a space
					if(strcmp(buff[j], " ")){
						buff[j] = 0;
						break;
					}
				}
				pixmap[i].g = atoi(buff);
				
			//check to make sure no colors exceed the given max value
			if(pixmap[i].g > maxvalue | pixmap[i].r > maxvalue | pixmap[i].b > maxvalue){
				fprintf(stderr, "Error: some values exceed the maximum value");
			}
		}
	}
	
	//if file format is P6
	else if (strcmp(mn, "6")){
		char* pixmap = malloc(sizeof(width * height));
		
		// read in 3 bytes per pixel for the whole file
		fread(buffer, 3, width * height, fh);
		
		
	}
	
	//handle error in case it is not p3 or p6
	else {
		fprintf(stderr, "Error: This program will only convert either P3 or P6 ppm file formats, enter a valid file.");
		exit(0);
	}
	
	
	//check which format to convert to
	first_arg = atoi(argv[1]);
	
	//call the conversion to p3 if 3 entered
	if(first_arg == 3){
		p3_convert(pixmap, argv[3], width, height, maxvalue);
	}
	
	//call conversion to p6 if 6 entered as first argument
	else if (first_arg == 6){
		p6_convert(pixmap, argv[3], width, height, maxvalue);
	}
	
	//if neither of these entered as first param, call error
	else{
		fprintf(stderr, "Error: First argument must be either 3 or 6");
		exit(0);
	}
}